const base_url = 'https://spcart.hostx1.de'

const Endpoints = {
  GET_OTP() {
    return `${base_url}/api/v1/customer/send-otp`
  },
  VALIDATE_OTP () {
    return `${base_url}/api/v1/customer/validate-otp`
  },
  ACCOUNT_DETAIL () {
    return `${base_url}/api/v1/customer/account-details`
  },
  
};

export default Endpoints;