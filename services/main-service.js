/* eslint-disable */
"use strict";

import URLS from './endpoint.service';
import axios from "axios";


const MainService = {
    getOtp(params = {}) {
        
        return axios.post(URLS.GET_OTP(), params);
    },
    validateOtp(params = {}) {
        
        return axios.post(URLS.VALIDATE_OTP(), params);
    },
    accountDetail(params = {}) {
        
        return axios.post(URLS.ACCOUNT_DETAIL(), params);
    },
   
}

export default MainService;